import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.scss'],
})
export class GameMenuComponent {

  constructor(
    private popoverController: PopoverController,
  ) { }

  async onRestartGameClick() {
    await this.popoverController.dismiss(0);
  }

  async onResetBestTimeClick() {
    await this.popoverController.dismiss(1);
  }
}
