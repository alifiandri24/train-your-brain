export enum CardState {
  Untouched,
  Front,
  Back,
  Hidden,
}

export enum CardType {
  None,
  Image,
  Character,
}

export class Card {
  id: number = -1;
  state: CardState = CardState.Untouched;
  type: CardType = CardType.None;
  content: string = '';  // may contain an image URL or a letter
  options: any;

  constructor(id: number, type: CardType, content: string, options?: any) {
    this.id = id;
    this.type = type;
    this.content = content;
    this.options = options;
  }
}
