import { CardType } from '../entities/card';
import { CardContent } from '../interfaces/card-content';

export interface CardTheme {
	id: string;
	title: string;
	titleShort: string;
	image: string;
	type: CardType,
	contents?: CardContent[];
	directory?: string;
	options?: any;
}
