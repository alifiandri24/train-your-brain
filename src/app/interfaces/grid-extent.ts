export interface GridExtent {
	rows: number;
	cols: number;
	image: string;
}
