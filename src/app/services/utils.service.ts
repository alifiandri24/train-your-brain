import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  static copyObject(obj: any): any {
    return JSON.parse(JSON.stringify(obj));
  }

  static pad(num: number, size: number) {
    let s: string = num + '';
    while (s.length < size) {
      s = "0" + s;
    }
    return s;
  }

  static formatTime(timeInSeconds: number): string {
    let minutes: number = Math.floor(timeInSeconds / 60);
    let seconds: number = timeInSeconds - minutes * 60;
    return this.pad(minutes, 2) + ':' + this.pad(seconds, 2);
  }

  // Returns a random integer between min (include) and max (include)
  static random(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
