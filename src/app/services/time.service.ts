import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  private currentTime: number = 0;
  private currentBestTime: number = 0;
  private timer: any = null;

  // Constants
  private TIMER_INTERVAL: number = 1000;

  constructor() { }

  resetTimer() {
    this.stopTimer();
    this.currentTime = 0;
  }

  resetBestTime() {
    this.currentBestTime = 0;
  }

  startTimer() {
    this.timer = setInterval(() => {
      this.currentTime++;
    }, this.TIMER_INTERVAL);
  }

  stopTimer() {
    clearInterval(this.timer);
  }

  saveBestTime(themeId: string, rows: number, cols: number) {
    let storageId: string = this.getLocalStorageId(themeId, rows, cols);
    localStorage.setItem(storageId, this.currentTime.toString());
  }

  loadBestTime(themeId: string, rows: number, cols: number) {
    // Returns with the timein seconds. 0 if not yet stored a value for this ID.
    let storageId: string = this.getLocalStorageId(themeId, rows, cols);
    let timeInSeconds: string = localStorage.getItem(storageId);
    this.currentBestTime = parseInt(timeInSeconds) || 0;
  }

  removeBestTime(themeId: string, rows: number, cols: number) {
    let storageId: string = this.getLocalStorageId(themeId, rows, cols);
    localStorage.removeItem(storageId);
  }

  clearAllBestTimes() {
    localStorage.clear();
  }

  get CurrentTimeStr(): string {
    return UtilsService.formatTime(this.currentTime);
  }

  get CurrentBestTimeStr(): string {
    return UtilsService.formatTime(this.currentBestTime);
  }

  get IsBestTime(): boolean {
    return this.currentBestTime <= 0 || this.currentTime < this.currentBestTime;
  }

  private getLocalStorageId(themeId: string, rows: number, cols: number): string {
    return `${themeId},${rows},${cols}`;
  }
}
