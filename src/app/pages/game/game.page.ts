import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router"
import { PopoverController, ToastController } from '@ionic/angular';
import { CardTheme } from '../../interfaces/card-theme';
import { GridExtent } from '../../interfaces/grid-extent';
import { Card, CardState, CardType } from '../../entities/card';
import { GameService } from '../../services/game.service';
import { GridService } from '../../services/grid.service';
import { TimeService } from '../../services/time.service';
import { GameMenuComponent } from '../../components/game-menu/game-menu.component';
import * as CardData from '../../data/card-data';

@Component({
  selector: 'app-game',
  templateUrl: 'game.page.html',
  styleUrls: ['game.page.scss'],
})
export class GamePage implements OnInit, OnDestroy {

  CardState = CardState;
  CardType = CardType;

  grid: Card[][];

  // Currently selected cards
  firstCard: Card;
  secondCard: Card;

  private isChecking: boolean = false;
  private timers: any[] =[];

  // Constants
  private CHECK_TIMEOUT: number     = 2000;
  private FLIP_TIMEOUT: number      = 300;
  private HIDE_TIMEOUT: number      = 500;
  private COMPLETED_TIMEOUT: number = 1000;

  constructor(
    private router: Router,
    private gameService: GameService,
    private popoverController: PopoverController,
    private toastController: ToastController,
    public gridService: GridService,
    public timeService: TimeService
  ) { }

  ngOnInit() {
    this.startGame();
  }

  ngOnDestroy() {
    this.clearTimeouts();
  }

  onCardClick(card: Card) {
    if (card && card.state != CardState.Hidden && !this.isChecking) {
      // Select first card
      if (this.firstCard == null) {
        this.firstCard = card;
        this.flipCard(card);
      // Select second card
      } else if (this.secondCard == null && card != this.firstCard) {
        this.secondCard = card;
        this.flipCard(card);
        this.checkSelectedCards();
      }
    }
  }

  onBackClick() {
    this.router.navigate(['/'], { replaceUrl: true });
  }

  async onMenuClick() {
    const popover = await this.popoverController.create({
      component: GameMenuComponent,
      event: event,
      cssClass: 'popover-custom',
      showBackdrop: false,
    });
    // Fires after popover closed
    popover.onDidDismiss().then((result: any) => {
      switch (result.data) {
        case 0:
          this.restartGame();
          break;
        case 1:
          this.resetBestTime();
          break;
      }
    });
    return await popover.present();
  }

  getCardImageUrl(card: Card) {
    return `url('${CardData.CARD_IMAGES_DIR}${this.CardTheme.directory}/${card.content}${CardData.CARD_IMAGES_EXTENSION}')`;
  }

  // Shortcut to game service
  get GridExtent(): GridExtent {
    return this.gameService.CurrentGridExtent;
  }

  // Shortcut to game service
  get CardTheme(): CardTheme {
    return this.gameService.CurrentCardTheme;
  }

  private startGame() {
    this.clearTimeouts();
    // Creating grid with cards
    this.grid = this.gridService.createGrid(this.GridExtent, this.CardTheme);
    // Load best time if exists
    this.timeService.loadBestTime(this.CardTheme.id, this.GridExtent.rows, this.GridExtent.cols);
    // Start timer
    this.timeService.resetTimer();
    this.timeService.startTimer();
    // Reset variables
    this.isChecking = false;
    this.firstCard = null;
    this.secondCard = null;
  }

  private flipCard(...cards: Card[]) {
    cards.forEach((card) => {
      if (card) {
        switch (card.state) {
          case CardState.Untouched:
            card.state = CardState.Front;
            break;
          case CardState.Front:
            card.state = CardState.Back;
            break;
          case CardState.Back:
            card.state = CardState.Front;
            break;
        }
      }
    });
  }

  private hideCard(...cards: Card[]) {
    cards.forEach((card) => {
      if (card) {
        card.state = CardState.Hidden;
      }
    });
  }

  private checkSelectedCards() {
    this.clearTimeouts();
    if (this.firstCard && this.secondCard) {
      this.isChecking = true;
      this.timers.push(setTimeout(() => {
        // The two cards are pairs
        if (this.firstCard.id == this.secondCard.id) {
          this.hideCard(this.firstCard, this.secondCard);
          this.timers.push(setTimeout(() => {
            this.firstCard = null;
            this.secondCard = null;
            this.isChecking = false;
            // Check grid
            if (this.checkGridCompleted()) {
              this.timeService.stopTimer();
              // Save best time
              if (this.timeService.IsBestTime) {
                this.timeService.saveBestTime(this.CardTheme.id, this.GridExtent.rows, this.GridExtent.cols);
              }
              this.timers.push(setTimeout(() => {
                this.router.navigate(['/completed'], { replaceUrl: true }); // navigate to Completed page (without back in history)
              }, this.COMPLETED_TIMEOUT));
            }
          }, this.HIDE_TIMEOUT));
        // Differs
        } else {
          this.flipCard(this.firstCard, this.secondCard);
          this.timers.push(setTimeout(() => {
            this.firstCard = null;
            this.secondCard = null;
            this.isChecking = false;
          }, this.FLIP_TIMEOUT));
        }
      }, this.CHECK_TIMEOUT));
    }
  }

  private checkGridCompleted(): boolean {
    for (let row = 0; row < this.GridExtent.rows; row++) {
      for (let col = 0; col < this.GridExtent.cols; col++) {
        if (this.grid[row][col].type != CardType.None && this.grid[row][col].state != CardState.Hidden) {
          return false; // stop if one of the cards is not hidden
        }
      }
    }
    return true;
  }

  private restartGame() {
    this.startGame();
    this.presentToast('Game restarted');
  }

  private resetBestTime() {
    this.timeService.removeBestTime(this.CardTheme.id, this.GridExtent.rows, this.GridExtent.cols);
    this.timeService.resetBestTime();
    this.presentToast('Best time set to zero');
  }

  private clearTimeouts() {
    for (let i = 0; i < this.timers.length; i++) {
      clearTimeout(this.timers[i]);
    }
    this.timers = [];
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'middle',
      color: 'light',
      cssClass: 'toast-custom'
    });
    toast.present();
  }
}
