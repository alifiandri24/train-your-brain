import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { GamePage } from './game.page';

import { ComponentsModule } from '../../components/components.module';
import { GameMenuComponent } from '../../components/game-menu/game-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: GamePage
      }
    ])
  ],
  declarations: [GamePage],
  entryComponents: [
    GameMenuComponent
  ]
})
export class GamePageModule {}
