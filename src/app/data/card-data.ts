import { CardType } from '../entities/card';
import { CardContent } from '../interfaces/card-content';
import { CardTheme } from '../interfaces/card-theme';

// NOTE: Full path of an image is '/assets/images/cards/nature/image.png'
export const CARD_IMAGES_DIR: string = '/assets/images/cards';
export const CARD_IMAGES_EXTENSION: string = '.png';
export const CARD_SILHOUETTES_EXTENSION: string = '.sil';
export const NATURE_IMAGES_DIR: string = '/nature';
export const OBJECT_IMAGES_DIR: string = '/objects';

export const NATURE_IMAGES: CardContent[] = [
  { first: 'ant' },
  { first: 'apple' },
  { first: 'banana' },
  { first: 'bee' },
  { first: 'butterfly' },
  { first: 'carrot' },
  { first: 'cat' },
  { first: 'cherry' },
  { first: 'dolphin' },
  { first: 'elephant' },
  { first: 'frog' },
  { first: 'grape' },
  { first: 'ladybug' },
  { first: 'leaf' },
  { first: 'mouse' },
  { first: 'mushroom' },
  { first: 'orange' },
  { first: 'pear' },
  { first: 'pine' },
  { first: 'pineapple' },
  { first: 'potatoe' },
  { first: 'sheep' },
  { first: 'strawberry' },
  { first: 'tiger' },
  { first: 'tomato' },
  { first: 'tree' },
  { first: 'watermelon' },
];

export const OBJECT_IMAGES: CardContent[] = [
  { first: 'anchor' },
  { first: 'baloon' },
  { first: 'box' },
  { first: 'car' },
  { first: 'cup' },
  { first: 'clock' },
  { first: 'cloud' },
  { first: 'dice' },
  { first: 'earth' },
  { first: 'egg' },
  { first: 'fire' },
  { first: 'football' },
  { first: 'guitar' },
  { first: 'hamburger' },
  { first: 'hat' },
  { first: 'heart' },
  { first: 'house' },
  { first: 'ice-cream' },
  { first: 'jam' },
  { first: 'key' },
  { first: 'kite' },
  { first: 'lamp' },
  { first: 'newspaper' },
  { first: 'pencil' },
  { first: 'rainbow' },
  { first: 'robot' },
  { first: 'ship' },
  { first: 'shoes' },
  { first: 'toaster' },
  { first: 'umbrella' },
  { first: 'windmill' },
  { first: 'vase' },
  { first: 'yoyo' },
  { first: 'zipper' },
];

// Arrays created by GameService
export const NATURE_IMAGE_PAIRS: CardContent[] = [];
export const OBJECT_IMAGE_PAIRS: CardContent[] = [];
export const LOWER_CASE: CardContent[] = [];
export const UPPER_CASE: CardContent[] = [];
export const UPPER_LOWER_CASE: CardContent[] = [];
export const NUMBERS: CardContent[] = [];

// Theme data
export const CARD_THEMES: CardTheme[] = [
  {
    id: 'nature',
    title: 'Nature images',
    titleShort: 'Nature',
    image: 'nature-images',
    type: CardType.Image,
    contents: NATURE_IMAGES,
    directory: NATURE_IMAGES_DIR
  },
  {
    id: 'nature_sil',
    title: 'Nature silhouettes',
    titleShort: 'Nature',
    image: 'nature-image-pairs',
    type: CardType.Image,
    contents: NATURE_IMAGE_PAIRS,
    directory: NATURE_IMAGES_DIR
  },
  {
    id: 'objects',
    title: 'Object images',
    titleShort: 'Objects',
    image: 'object-images',
    type: CardType.Image,
    contents: OBJECT_IMAGES,
    directory: OBJECT_IMAGES_DIR
  },
  {
    id: 'objects_sil',
    title: 'Object silhouettes',
    titleShort: 'Objects',
    image: 'object-image-pairs',
    type: CardType.Image,
    contents: OBJECT_IMAGE_PAIRS,
    directory: OBJECT_IMAGES_DIR
  },
  {
    id: 'upper_case',
    title: 'Upper case',
    titleShort: 'Upper case',
    image: 'upper-case',
    type: CardType.Character,
    contents: UPPER_CASE,
    options: {
      randomColors: true
    }
  },
  {
    id: 'lower_case',
    title: 'Lower case',
    titleShort: 'Lower case',
    image: 'lower-case',
    type: CardType.Character,
    contents: LOWER_CASE,
    options: {
      randomColors: true
    }
  },
  {
    id: 'upper_lower_case',
    title: 'Letter pairs',
    titleShort: 'Letter pairs',
    image: 'upper-lower-case',
    type: CardType.Character,
    contents: UPPER_LOWER_CASE
  },
  {
    id: 'numbers',
    title: 'Numbers',
    titleShort: 'Numbers',
    image: 'numbers',
    type: CardType.Character,
    contents: NUMBERS,
    options: {
      randomColors: true
    }
  },
];
