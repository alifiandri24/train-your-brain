# Train Your Brain

![Screenshot 00](misc/screenshots/00.jpg) ![Screenshot 01](misc/screenshots/01.jpg) ![Screenshot 02](misc/screenshots/02.jpg)

## About

A simple memory matching game written in *Angular/Ionic* as a PWA (progressive web application).  

## Application

A playable version of the game can be found here:

[train-your-brain.konyisoft.eu](https://train-your-brain.konyisoft.eu/)

## Install

To try out this project, first you will have [Node.js](https://nodejs.org/en/) and [Git](https://git-scm.com/) installed on your computer.

Download (clone) the files of the project to your local computer:

```git clone git@gitlab.com:konyisoft/train-your-brain.git```

Navigate to the */train-your-brain* directory and run the following command to install the packages required by the project:

```npm install```

Finally, open the project in your default browser with this command:

```ionic serve```

## License

MIT

----

Krisztian Konya, Konyisoft, 2020  
[konyisoft.eu](https://konyisoft.eu/)
